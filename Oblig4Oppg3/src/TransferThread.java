class TransferThread extends Thread
{  
	public TransferThread(Bank b, int from, int max)
		  { 
		   bank = b;
		   fromAccount = from;
		   maxAmount = max;
		  }
		
		  public void run()
		  { 
		   try
		   { 
		     while (!interrupted())
		     { 
		      int toAccount = (int)(bank.size() * Math.random());
		      int amount = (int)(maxAmount * Math.random());
		      bank.transfer(fromAccount, toAccount, amount);
		      sleep(1);
		     }
		   }
		   catch(InterruptedException e) {}
		  }
		
		  private Bank bank;
		  private int fromAccount;
		  private int maxAmount;


}

