
import java.util.Scanner; 

public class TowersOfHanoi {
	 static int countOfMoves=0;
	 static int countOfCalls=0;
  public static void main(String[] args) {
    // Create a Scanner
    Scanner input = new Scanner(System.in);
    System.out.print("Skriv det antallet av disker du �nsker: ");
    int n = input.nextInt();

    // Find the solution recursively
   
    System.out.println("Flyttningene er:");
    moveDisks(n, 'A', 'B', 'C');
    
    System.out.println("antall flyttninger er:"+countOfMoves);
    System.out.println("antall metode kall: "+countOfCalls);
  }

  /** The method for finding the solution to move n disks
      from fromTower to toTower with auxTower */
  public static void moveDisks(int n, char fromTower,
      char toTower, char auxTower) {
	  countOfMoves++;
	  countOfCalls++;
    if (n == 1) // Stopping condition
      System.out.println("Flytt disk " + n + " fra " +
        fromTower + " til " + toTower);
    else {
      moveDisks(n - 1, fromTower, auxTower, toTower);
      System.out.println("flytt disk " + n + " fra " +
        fromTower + " til " + toTower);
      moveDisks(n - 1, auxTower, toTower, fromTower);
     
      
    }
  }
}